package ro.tuc.ds2020.web_socket;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import ro.tuc.ds2020.my_logger.MyLoggeer;

import java.util.Map;

@Controller
@RequestMapping(value = "/api/v1/chat")
public class WebSocketController {
    static public final String TOPIC = "/topic/chat";
    private final SimpMessagingTemplate template;

    WebSocketController(SimpMessagingTemplate template){
        this.template = template;
    }

    @PostMapping("/send")
    public ResponseEntity<Void> sendMessage(@RequestBody WebSocketMessage message) {
        template.convertAndSend(TOPIC, message);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @MessageMapping("/hello")
    @SendTo(TOPIC)
    public WebSocketMessage greeting(WebSocketMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new WebSocketMessage("New message: "
                + HtmlUtils.htmlEscape(message.getContent()));
    }


    @MessageMapping("/all")
    @SendTo("/topic/all")
    public Map<String, String> messageToAll(@Payload Map<String, String> message) {
        MyLoggeer.print("Message received: " + message);
        message.put("timestamp", Long.toString(System.currentTimeMillis()));
        return message;
    }

    @MessageMapping("/to_admin")
    @SendTo("/topic/admin")
    public Map<String, String> messageToAdmin(@Payload Map<String, String> message) {
        MyLoggeer.print("Message received: " + message);
        message.put("timestamp", Long.toString(System.currentTimeMillis()));
        return message;
    }

    @MessageMapping("/to_user")
    public Map<String, String> messageToUser(@Payload Map<String, String> message) {
        MyLoggeer.print("Message received: " + message);
        message.put("timestamp", Long.toString(System.currentTimeMillis()));
        String userName = message.get("receiver");
        template.convertAndSend("/queue/user/" + userName, message);
        template.convertAndSend("/queue/admin", message);
        return message;
    }

    @MessageMapping("/user-message")
    @SendToUser("/queue/reply")
    public String sendBackToUser(@Payload String message /*, @Header("simpSessionId") String sessionId*/) {
        return "Only you have received this message: " + message;
    }

    @MessageMapping("/user-message-{userName}")
    public void sendToOtherUser(@RequestBody String message,
                                @PathVariable("userName") @DestinationVariable String userName) {
        template.convertAndSend("/queue/reply-" + userName, "You have a message from someone: " + message);
    }

    @PostMapping("/sendToUser/{userName}")
    public ResponseEntity<Void>  sendToUser(@RequestBody WebSocketMessage message, @PathVariable("userName") String userName) {
        MyLoggeer.print(userName);
        template.convertAndSend("/queue/user/" + userName, message);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}