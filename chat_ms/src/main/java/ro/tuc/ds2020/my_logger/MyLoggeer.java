package ro.tuc.ds2020.my_logger;

abstract public class MyLoggeer {
    static boolean enabled = true;

    public static void print(String value) {
        if (enabled) {
            System.out.println("/MyLoggeer: " + value);
        }
    }
}
